import pytest
from test_project import utils

user_agent_mobile = "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) " \
                    "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Mobile Safari/537.36"
user_agent_desktop = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)" \
                     " Chrome/51.0.2704.103 Safari/537.36"


@pytest.fixture(scope="class")
def base_url_init(request):
    request.cls.base_url = request.config.getoption("--url")
    yield


@pytest.mark.usefixtures("base_url_init")
class TestAPI:

    # def setup_class(self):
    #     assert requests.get(self.base_url, timeout=10)  # check service availability
    #     assert utils.post(url=self.base_url, user_action="10").status_code == 200  # check backend availability
    #     assert utils.pg_query()  # check db availability

    @pytest.fixture(scope="function", params=list(range(0, 11)))
    def param_test_expected_user_actions(self, request):
        return request.param

    def test_expected_user_actions_value(self, param_test_expected_user_actions):
        utils.post(url=self.base_url, user_action=str(param_test_expected_user_actions))

        result = utils.pg_query()

        assert result["result"] == param_test_expected_user_actions

    def test_expected_user_actions_status_ok(self, param_test_expected_user_actions):
        result = utils.post(url=self.base_url, user_action=str(param_test_expected_user_actions)).json()

        assert result['status'] == "ok"

    @pytest.fixture(scope="function", params=['-1', '11', 'A', 'Б', '?', '☺'])
    def param_test_unexpected_user_actions(self, request):
        return request.param

    def test_unexpected_user_actions_http_status_code(self, param_test_unexpected_user_actions):
        user_action = param_test_unexpected_user_actions

        resp = utils.post(url=self.base_url, user_action=user_action)

        assert resp.status_code == 400

    def test_unexpected_user_actions_status_error(self, param_test_unexpected_user_actions):
        result = utils.post(url=self.base_url, user_action=str(param_test_unexpected_user_actions)).json()

        assert result['status'] == "error"

    def test_unexpected_user_actions_error_message(self, param_test_unexpected_user_actions):
        result = utils.post(url=self.base_url, user_action=str(param_test_unexpected_user_actions)).json()

        assert result['errors']

    def test_unexpected_user_actions_value_write_to_db(self, param_test_expected_user_actions):
        user_action = param_test_expected_user_actions

        result_before_test = utils.pg_query()
        utils.post(url=self.base_url, user_action=user_action)
        result_after_test = utils.pg_query()

        assert result_before_test == result_after_test

    @pytest.fixture(scope="function", params=[
        '[!@#$%^&*(),.?":{}|<>]',
        'Фидбек на кириллице',
        'Latin feedback',
        ''.join(str(i) for i in range(255)),
    ])
    def param_test_feedback_value(self, request):
        return request.param

    def test_feedback_value(self, param_test_feedback_value):
        feedback = param_test_feedback_value

        utils.post(url=self.base_url, user_action="10", feedback=feedback)
        result = utils.pg_query()

        assert result["feedback"] == feedback

    def test_user_action_argument_required_http_status(self):
        result = utils.post(url=self.base_url, user_action=None)

        assert result.status_code == 400

    def test_user_action_argument_required_write_to_db(self):
        result_before_test = utils.pg_query()
        utils.post(url=self.base_url, user_action=None)
        result_after_test = utils.pg_query()

        assert result_before_test == result_after_test

    def test_feedback_argument_not_required_http_status(self):
        result = utils.post(url=self.base_url, user_action="10", feedback=None)

        assert result.status_code == 200

    def test_feedback_argument_not_required_write_to_db(self):
        result_before_test = utils.pg_query()
        utils.post(url=self.base_url, user_action="10", feedback=None)
        result_after_test = utils.pg_query()

        assert result_before_test != result_after_test

    def test_user_agent_desktop(self):
        utils.post(url=self.base_url, user_action="10", user_agent=user_agent_desktop)
        result = utils.pg_query()

        assert result['device'] == 'desktop'

    def test_user_agent_mobile(self):
        utils.post(url=self.base_url, user_action="10", user_agent=user_agent_mobile)
        result = utils.pg_query()

        assert result['device'] == 'mobile'
