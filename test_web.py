import pytest
import os
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, ElementNotVisibleException, ElementNotInteractableException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from test_project import utils

nps_window = '//div[@class="NPS"]'
nps_close_button = '//div[@class="NPS__close"]'
nps_send_feedback_button = '//button[@class="NPS__feedback-send"]'
nps_result_button = '//div[@class="NPS__button n{}"]'
nps_feedback_text_area = '//textarea[@class="NPS__feedback-textarea"]'


@pytest.fixture(params=["chrome", "firefox"], scope="class")
def driver_init(request):
    if request.param == "chrome":
        driver = webdriver.Chrome(
            os.path.join(os.path.dirname(os.path.abspath(__file__)), "driver", "chromedriver.exe"))
    if request.param == "firefox":
        driver = webdriver.Firefox(
            executable_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "driver", "geckodriver.exe")
        )
    request.cls.driver = driver
    yield
    driver.close()


@pytest.fixture(scope="class")
def base_url_init(request):
    request.cls.base_url = request.config.getoption("--url")
    yield


@pytest.fixture(scope="function")
def function_setup(request):
    driver = request.cls.driver

    wait = WebDriverWait(driver, 2)
    driver.get(request.cls.base_url)
    try:
        wait.until(EC.presence_of_element_located((By.XPATH, nps_window)))
    except (TimeoutException, ElementNotVisibleException):
        TestWebNPS().set_nps_cookie("0")
        TestWebNPS().execute_nps_script()
        wait.until(EC.presence_of_element_located((By.XPATH, nps_window)))

    def function_teardown():
        try:
            wait.until(EC.presence_of_element_located((By.XPATH, nps_close_button))).click()
        except (TimeoutException, ElementNotVisibleException, ElementNotInteractableException):
            pass

    request.addfinalizer(function_teardown)


@pytest.mark.usefixtures("driver_init")
@pytest.mark.usefixtures("base_url_init")
@pytest.mark.usefixtures("function_setup")
class TestWebNPS:

    def execute_nps_script(self, show_rate=1):
        nps_script = """    
            NPS({
                showRate: %s,
                message: 'How likely are you to recommend our website to friend?',
                notLikeTitle: 'Not at all likely',
                likeTitle: 'Extremely likely',
                isFeedbackEnabled: true,
                endpoint: '/nps',
                feedbackTitle: 'Would you like write a message about your opinion?',
            });
        """ % show_rate
        self.driver.execute_script(nps_script)

    def set_nps_cookie(self, value):
        while self.get_cookie()['NPS_sended'] != str(value):
            self.driver.execute_script(f'document.cookie="NPS_sended={value}"')

    def get_cookie(self):
        cookies_list = self.driver.get_cookies()
        cookies_dict = {}
        for cookie in cookies_list:
            cookies_dict[cookie['name']] = cookie['value']
        return cookies_dict

    def send_result(self, result, feedback=None):
        wait = WebDriverWait(self.driver, 2)
        result_button = wait.until(EC.presence_of_element_located((By.XPATH, nps_result_button.format(result))))
        result_button.click()

        if result < 6:
            send_feedback_button = wait.until(EC.presence_of_element_located((By.XPATH, nps_send_feedback_button)))
            if feedback:
                feedback_text_area = wait.until(EC.presence_of_element_located((By.XPATH, nps_feedback_text_area)))
                feedback_text_area.send_keys(feedback)
            send_feedback_button.click()

    def wait_for_window_native_show(self):
        wait = WebDriverWait(self.driver, 2)

        retry_max = 20
        while retry_max > 0:
            self.set_nps_cookie(value=0)
            self.driver.get(self.base_url)
            try:
                wait.until(EC.presence_of_element_located((By.XPATH, nps_window)))
                break
            except (TimeoutException, ElementNotVisibleException):
                retry_max -= 1

    def test_execute_nps_script(self):
        wait = WebDriverWait(self.driver, 2)
        window = wait.until(EC.presence_of_element_located((By.XPATH, nps_window)))
        assert window.is_displayed()

    def test_nps_sended_cookie_is_set_after_show_window(self):
        cookie = self.get_cookie()
        assert cookie['NPS_sended'] == "1"

    @pytest.mark.parametrize("button_id", list(range(0, 11)))
    def test_nps_sended_cookie_is_set_after_send_result(self, button_id):
        self.send_result(result=button_id)

        cookie = self.get_cookie()
        assert cookie['NPS_sended'] == "1"

    @pytest.mark.parametrize("button_id", list(range(0, 11)))
    def test_result_send_value(self, button_id):
        self.send_result(result=button_id)

        result = utils.pg_query()
        assert result["result"] == button_id

    @pytest.mark.parametrize("feedback", [
        '[!@#$%^&*(),.?":{}|<>]',
        'Фидбек на кириллице',
        'Latin feedback',
        ''.join(str(i) for i in range(255)),
    ])
    def test_feedback_send_value(self, feedback):
        self.send_result(result=1, feedback=feedback)

        result = utils.pg_query()
        assert result["feedback"] == feedback

    def test_feedback_argument_not_required_write_to_db(self):
        feedback = None
        self.send_result(result=1, feedback=feedback)

        result = utils.pg_query()
        assert result["feedback"] == feedback

    def test_cookie_is_set__window_not_show(self):
        wait = WebDriverWait(self.driver, 2)

        self.driver.get(self.base_url)
        self.set_nps_cookie(value=1)
        self.execute_nps_script(show_rate=0)

        try:
            window = wait.until(EC.presence_of_element_located((By.XPATH, nps_window)))
        except (TimeoutException, ElementNotVisibleException):
            window = False

        assert not window

    def test_cookie_not_set__window_not_show__cookie_is_set(self):
        wait = WebDriverWait(self.driver, 2)

        self.driver.get(self.base_url)
        self.set_nps_cookie(value=0)
        self.execute_nps_script(show_rate=0)

        try:
            wait.until(EC.presence_of_element_located((By.XPATH, nps_window)))
        except (TimeoutException, ElementNotVisibleException):
            pass

        cookie = self.get_cookie()
        assert cookie['NPS_sended'] == "1"

    def test_cookie_not_set__window_show__cookie_is_set(self):
        wait = WebDriverWait(self.driver, 2)
        wait.until(EC.presence_of_element_located((By.XPATH, nps_close_button))).click()
        wait.until(EC.invisibility_of_element_located((By.XPATH, nps_window)))

        self.set_nps_cookie(value=0)
        self.execute_nps_script(show_rate=1)

        try:
            wait.until(EC.presence_of_element_located((By.XPATH, nps_window)))
        except (TimeoutException, ElementNotVisibleException):
            pass

        cookie = self.get_cookie()
        assert cookie['NPS_sended'] == "1"

    @pytest.mark.parametrize("class_name, expected_text", [
        ("NPS__message", "How likely are you to recommend our website to friend?"),
        ("NPS__not-like-title", "Not at all likely"),
        ("NPS__like-title", "Extremely likely")]
                             )
    def test_main_window_text(self, class_name, expected_text):
        wait = WebDriverWait(self.driver, 2)
        wait.until(EC.presence_of_element_located((By.XPATH, nps_close_button))).click()
        wait.until(EC.invisibility_of_element_located((By.XPATH, nps_window)))

        self.wait_for_window_native_show()

        message = wait.until(EC.presence_of_element_located((By.XPATH, f'//div[@class="{class_name}"]')))

        assert message.text == expected_text

    @pytest.mark.parametrize("class_name, expected_text", [
        ("NPS__feedback-title", "Would you like write a message about your opinion?"),
        ("NPS__feedback-send", "SEND")]
                             )
    def test_feedback_window_text(self, class_name, expected_text):
        wait = WebDriverWait(self.driver, 2)
        wait.until(EC.presence_of_element_located((By.XPATH, nps_close_button))).click()
        wait.until(EC.invisibility_of_element_located((By.XPATH, nps_window)))

        self.wait_for_window_native_show()

        result_button = wait.until(EC.presence_of_element_located((By.XPATH, nps_result_button.format(0))))
        result_button.click()

        message = wait.until(EC.presence_of_element_located((By.XPATH, f'//*[@class="{class_name}"]')))

        assert message.text == expected_text
