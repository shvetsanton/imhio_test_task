import sys
import psycopg2
import requests
from contextlib import closing
from psycopg2.extras import DictCursor


def pg_query(query: str = 'SELECT * FROM t_feedback_models ORDER BY ID DESC LIMIT 1;'):
    try:
        conn = psycopg2.connect(dbname="core", user="postgres", password="devpass", host="127.0.0.1",
                                port="5433")
        with closing(conn) as conn:
            with conn.cursor(cursor_factory=DictCursor) as cursor:
                cursor.execute(query)
                query_result = list(cursor)
                if not query_result:
                    return query_result
                column_names = [desc[0] for desc in cursor.description]
                column_values = query_result[len(query_result) - 1]
        return dict(zip(column_names, column_values))
    except psycopg2.OperationalError as err:
        print(f'Unable to connect!\n{err}')
        sys.exit(1)


def post(url, user_action, feedback=None, user_agent=""):
    headers = {'User-Agent': user_agent}
    data = {"user_action": user_action, "feedback": feedback}

    if user_action is None:
        del data["user_action"]
    if feedback is None:
        del data["feedback"]

    resp = requests.post(url=f"{url}/nps", headers=headers, json=data)

    return resp





