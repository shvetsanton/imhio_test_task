def pytest_addoption(parser):
    parser.addoption("--url", action="store", default="http://localhost:58001",
                     help="Type the base URL: http://someurl.com. Default is http://localhost:58001")


